# Zadanie rekrutacyjne

Przedstaw różnice w działaniu sieci energetycznej pomiędzy poszczególnymi godzinami za pomocą
wizualizacji w ReactJS.

## Analiza problemu

### Analiza danych

Zadanie warto rozpocząć od analizy danych znajdujących się w pliku [data](data.hdf5).<br />Wnioski i założenia:
* dane są kompletne, nie musimy zmagać się z sytuacją, w której brakuje np. danych dla danego węzła lub godziny,
* struktura sieci nie zmienia się w czase (zmieniaja się jedynie wartość 'flow' w 'branches', nie 'node_from' i 'node_to'), więc zakładamy, że jest stała
* 'type' w węźle nie zmienia się w czasie
* 'generation' w generatorze nie zmienia się w czasie, więc zakładamy, że jest stała,
* 'cost' w generatorze nie zmiania się w czasie, więc zakładamy, że jest stały,
* zakładamy, że 'node_type' w 'node' dla wartości równych 1 oznacza węzeł, który nie jest generatorem,
* zakładamy, że 'node_type' w 'node' dla wartości większych od 1 oznacza różne typy generatorów.

Biorąc pod uwagę, że operujemy na małym zbiorze danych może okazać się, że powyższe założenia nie zawsze są prawdziwe. W przypadku tworzenia profesjonalnego systemu należy o tym pamiętać.

### Schemat bazy danych

Schemat bazy danych znajduje się  w pliku [schemat](db_schema.pdf)

## Instrucja uruchomienia

Stwórz srodowisko

```
conda create --name electro
```

Zainstaluj zależności:

```
pip install -r requirements.txt
```

W katalogu 'web' wywołaj komendy

```
npm i
```
```
npm run build
```

Uruchom aplikację:

```
python3 app.py
```
Uwaga! Baza danych inicjowana jest przed pierwszym żądaniem i jest usuwana po zatrzymaniu serwera.