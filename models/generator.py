from db import db
from models.node import NodeModel

class GeneratorModel(NodeModel):
    __tablename__ = 'gen'

    node_id = db.Column(db.ForeignKey('node.id'), primary_key=True)
    generation = db.Column(db.Float(precision=8))
    cost = db.Column(db.Float(precision=8))
    
    __mapper_args__ = {
        "polymorphic_identity": True,
    }

    def __init__(self, node_id, type, generation, cost):
        super().__init__(node_id, type)
        self.generation = generation
        self.cost = cost
    
    def json(self):
        return {
            'id': self.id,
            'type': self.type,
            'generation': self.generation,
            'cost': self.cost
        }

    @classmethod
    def find_by_node_id(cls, node_id):
        return cls.query.filter_by(node_id=node_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()
