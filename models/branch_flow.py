from db import db


class BranchFlowModel(db.Model):
    __tablename__ = 'branch_flow'

    branch_id = db.Column(db.ForeignKey('branch.id'), primary_key=True)
    time = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Float(precision=8))

    branch = db.relationship('BranchModel')

    def __init__(self, branch_id, time, value):
        self.branch_id = branch_id
        self.time = time
        self.value = value

    def json(self):
            return {
            'time': self.time,
            'value': self.value,
        }
    
    @classmethod
    def find_by_branch_id(cls, branch_id):
        return cls.query.filter_by(branch_id=branch_id)

    @classmethod
    def find_by_branch_id_and_time(cls, branch_id, time):
        return cls.query.filter_by(branch_id=branch_id, time=time).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_all_by_time(cls, time):
        return cls.query.filter_by(time=time)
