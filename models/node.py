from db import db


class NodeModel(db.Model):
    __tablename__ = 'node'

    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.Integer)
    is_generator = db.Column(db.Boolean)
    demands = db.relationship("NodeDemandModel")


    __mapper_args__ = {
        "polymorphic_identity": False,
        "polymorphic_on": is_generator,
    }

    def __init__(self, id, type):
        self.id = id
        self.type = type

    def json(self):
        return {
            'id': self.id,
            'type': self.type,
        }

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()
