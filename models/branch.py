from db import db
from sqlalchemy import or_


class BranchModel(db.Model):
    __tablename__ = 'branch'

    id = db.Column(db.Integer, primary_key=True)
    source_node_id = db.Column(db.ForeignKey('node.id'))
    target_node_id = db.Column(db.ForeignKey('node.id'))
    flows = db.relationship("BranchFlowModel")

    def __init__(self, source_node_id, target_node_id):
        self.source_node_id = source_node_id
        self.target_node_id = target_node_id

    def json(self):
        return {
            'id': self.id,
            'source_node_id': self.source_node_id,
            'target_node_id': self.target_node_id,
        }
    
    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_by_source_and_target(cls, source_node_id, target_node_id):
        return cls.query.filter_by(source_node_id=source_node_id, target_node_id=target_node_id).first()

    @classmethod
    def find_by_source_or_target(cls, node_id):
        return cls.query.filter(or_(cls.source_node_id == node_id, cls.target_node_id == node_id)).all()

    @classmethod
    def find_all(cls):
        return cls.query.all()
