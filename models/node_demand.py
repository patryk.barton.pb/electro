from db import db


class NodeDemandModel(db.Model):
    __tablename__ = 'node_demand'

    node_id = db.Column(db.Integer, db.ForeignKey('node.id'), primary_key=True)
    time = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Float(precision=8))

    node = db.relationship('NodeModel')

    def __init__(self, node_id, time, value):
        self.node_id = node_id
        self.time = time
        self.value = value

    def json(self):
        return {
            'time': self.time,
            'value': self.value,
        }
    
    @classmethod
    def find_by_node_id(cls, node_id):
        return cls.query.filter_by(node_id=node_id)

    @classmethod
    def find_by_node_id_and_time(cls, node_id, time):
        return cls.query.filter_by(node_id=node_id, time=time).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_all_by_time(cls, time):
        return cls.query.filter_by(time=time)