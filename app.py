from flask import Flask, send_from_directory
from flask import jsonify
from flask_cors import CORS
import os
import atexit

from db import db
from create_db import create_db, remove_db

from models.node import NodeModel
from models.branch import BranchModel
from models.node_demand import NodeDemandModel
from models.branch_flow import BranchFlowModel

app = Flask(__name__, static_folder='web/build')
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PROPAGATE_EXCEPTIONS'] = True

@app.before_first_request
def init_db():
    create_db()

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def serve(path):
    if path != "" and os.path.exists(app.static_folder + '/' + path):
        return send_from_directory(app.static_folder, path)
    else:
        return send_from_directory(app.static_folder, 'index.html')

@app.route('/nodes/<time>')
def get_nodes(time):
    return jsonify([{
        'node': NodeModel.find_by_id(demand.node_id).json(),
        'demand': demand.json()
        } for demand in NodeDemandModel.find_all_by_time(int(time))]), 200

@app.route('/branches/<time>')
def get_branches(time):
    return jsonify([{
        'branch':  BranchModel.find_by_id(flow.branch_id).json(),
        'flow': flow.json()
        } for flow in BranchFlowModel.find_all_by_time(int(time))]), 200

@app.route('/node/<node_id>')
def get_node(node_id):
    return jsonify({
        'node': NodeModel.find_by_id(node_id).json(),
        'demands': [demand.json() for demand in NodeDemandModel.find_by_node_id(node_id)]
        }), 200

@app.route('/node/<node_id>/balance')
def get_node_branches(node_id):
    branches_data = list()
    branches = BranchModel.find_by_source_or_target(node_id)
    for branch in branches:
        branch_data = branch.json()
        branch_data['flows'] = list()
        flows = BranchFlowModel.find_by_branch_id(branch.id)
        for flow in flows:
            branch_data['flows'].append(flow.json())
        branches_data.append(branch_data)
    return jsonify({
        'node': NodeModel.find_by_id(node_id).json(),
        'demands': [demand.json() for demand in NodeDemandModel.find_by_node_id(node_id)],
        'branches': branches_data
    }), 200

@app.route('/branch/<branch_id>')
def get_branch(branch_id):
    return jsonify({
        'branch': BranchModel.find_by_id(branch_id).json(),
        'flows': [flow.json() for flow in BranchFlowModel.find_by_branch_id(branch_id)]
    }), 200

@app.route('/time_range')
def get_time_range():
    tmp_node = NodeModel.find_by_id(1)
    if not tmp_node:
        return jsonify({'timeRange': []}), 200
    return jsonify({
        'timeRange': [demand.time for demand in tmp_node.demands]
    }), 200

if __name__ == '__main__':
    db.init_app(app)
    atexit.register(remove_db)
    app.run(port=8080, debug=True)
