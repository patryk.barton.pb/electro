import React from 'react';

export default class Info extends React.Component {
  render() {
    return <div>
      <h4>About App</h4>
      Simple demo app of electrical network monitoring system.
      <h4>Guide</h4>
      <h5>Time</h5>
      Select time from 'Time' section to display a graph.<br />
      <h5>Net Graph</h5>
      Graph nodes:
      <ul>
        <li>circles - consumer nodes</li>
        <li>orange squares - generator nodes</li>
      </ul>
      Nodes colors:
      <ul>
        <li>blue - consumer node with demand greater than 0</li>
        <li>gray - consumer where demand is 0</li>
        <li>red - nodes where balance is not 0</li>
      </ul>
      Consumer nodes sizes depend on node demand.<br />
      Generators nodes sizes depend on power delivered to net (POWER_DELIVERED = GENERATION - DEMAND).<br />
      Branches width depend on power flow.<br /><br />
      Hover on nodes and edges shows more detailed tooltip.<br />
      Click on nodes and edges displays graphs below.
      <h5>Node Graphs</h5>
      Plots node demand and balance.
      <h5>Branch Graph</h5>
      Plots branch flow values.<br />
      Click on data in graph will display Net Graph with new hour value. 
    </div>
  }
}