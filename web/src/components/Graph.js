import React from 'react';
import ReactEcharts from 'echarts-for-react';


export default class Graph extends React.Component {

  graphClickHandler = ({name}) => {
    this.props.timeChangeHandler(parseInt(name));
  }

  getTooltip = (data) => {
    return `Hour: ${data.name}<br/>Value: ${data.data.toFixed(5)}MW`;
  }

  getOption = (data) => {
    const xData = [];
    const yData = [];
    data.map(el => {
      xData.push(el.time);
      yData.push(el.value);
    }) 
    return {
      xAxis: {
        type: 'category',
        data: xData,
        axisLabel: {
          formatter: '{value}h'
        },
      },
      yAxis: {
        type: 'value',
        axisLabel: {
          formatter: '{value} MW'
        },
      },
      series: [
        {
          type: 'line',
          data: yData,
        }
      ],
      tooltip: {
        show: true,
        formatter: this.getTooltip
      },
      animation: true,
    };
  };

  render() {
    const { data } = this.props;
    return <div className='graph-container'>
      <ReactEcharts
        ref={ref => { this.chartRef = ref }}
        style={{ height: "35vh", width: "100%" }}
        lazyUpdate={true}
        option={this.getOption(data)}
        onEvents={{'click': this.graphClickHandler}}
      />
    </div>;
  }
}