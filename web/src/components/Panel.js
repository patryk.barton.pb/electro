import React from 'react';

export default class Panel extends React.Component {
    render() {
      return <div className='panel'>
          <div className='panel-title'>{this.props.title}</div>
          <div>
            {this.props.children}
          </div>
      </div>;
    }
  }