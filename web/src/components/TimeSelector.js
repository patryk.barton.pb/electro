import React from 'react';
import { get } from '../utils/api';
import ProgessIndicator from './ProgessIndicator';
import { Form } from 'react-bootstrap';


export default class TimeSelector extends React.Component {

  state = {
    isLoading: true,
  }

  componentDidMount = () => {
    get('/time_range')
      .then(res => {
        this.setState({
          isLoading: false,
          timeRange: res.data.timeRange,
        });
      });
  }

  timeChangeHandler = event => {
    const selectedTime = event.target.value;
    this.props.timeChangeHandler(selectedTime);
  }

  render() {
    if (this.state.isLoading) {
      return <div>
        <ProgessIndicator />
      </div>
    }
    const { timeRange } = this.state; 
    return <div>
      <Form>
        <Form.Group>
          <Form.Control as='select' onChange={this.timeChangeHandler} defaultValue={''}>
            <option value='' disabled>Please, select time</option>
            {timeRange.map(time => <option key={time} value={time}>{time}</option>)}
          </Form.Control>
        </Form.Group>
      </Form>
    </div>;
  }
}