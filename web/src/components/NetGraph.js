import React from 'react';
import ReactEcharts from 'echarts-for-react';


export default class NetGraph extends React.Component {

  shouldComponentUpdate(nextProps) {
    const { graphNodes, graphEdges } = nextProps;
    this.chartRef.getEchartsInstance().setOption(this.getOption(graphNodes, graphEdges));
    return false;
  }

  getTooltip = ({data}) => {
    if (data.objectType === 'node') {
      return `Node ID: ${data.name}<br />
                      Demand: ${data.demand.toFixed(2)}MW<br />
                      Balance: ${data.balance.toFixed(2)}MW`;
    }
    else if (data.objectType === 'edge') {
      return `Source node ID: ${parseInt(data.source)}<br />
                      Target node ID: ${parseInt(data.target)}<br />
                      Flow: ${data.value.toFixed(5)}MW`;
    }
    else if (data.objectType === 'gen') {
      return `Node ID: ${data.name}<br />
                      Demand: ${data.demand.toFixed(2)}MW<br />
                      Generation: ${data.generation.toFixed(5)}MW<br />
                      Cost: ${data.cost.toFixed(2)}<br />
                      Balance: ${data.balance.toFixed(5)}`;
    }
    return 'No data';
  }

  getOption = (graphNodes, graphEdges) => {
    return {
      series: [
        {
          type: 'graph',
          layout: 'force',
          symbolSize: 20,
          roam: true,
          label: {
            show: true
          },
          data: graphNodes,
          links: graphEdges,
          force: {
            repulsion: 100,
            initLayout: 'circular',
            layoutAnimation: false,
          },
        }
      ],
      tooltip: {
        show: true,
        formatter: this.getTooltip,
      },
      animation: false,
    };
  };

  render() {
    const { graphNodes, graphEdges } = this.props;
    return <div className='graph-container'>
      <ReactEcharts
        ref={ref => {this.chartRef = ref}}
        style={{ height: "80vh", width: "100%" }}
        lazyUpdate={true}
        option={this.getOption(graphNodes, graphEdges)}
        onEvents={{'click': this.props.graphClickHandler}}
      />
    </div>;
  }
}