import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Panel from './components/Panel';
import TimeSelector from './components/TimeSelector';
import NetGraph from './components/NetGraph';
import Graph from './components/Graph';
import Info from './components/Info';

import {
  getGraphData,
  getNodeData,
  getBranchData,
  getNodeBalance
} from './utils/data_proccessing';

class App extends React.Component {

  state = {}

  timeChangeHandler = async (selectedTime) => {
    const { graphNodes, graphEdges } = await getGraphData(selectedTime);
    this.setState({
      graphNodes,
      graphEdges,
      selectedTime,
    });
  }

  graphClickHandler = async ({ data }) => {
    const { objectType } = data;
    let content = null;
    if (objectType === 'node' || objectType === 'gen') {
      const nodeData = await getNodeData(data.id);
      const nodeDemands = await getNodeBalance(data.id);
      content = <div>
        <Panel title={`Node with ID "${data.id}" demands`} key={`node-demands-${data.id}`}>
          <Graph data={nodeData.demands}
            timeChangeHandler={this.timeChangeHandler}
          />
        </Panel>
        <Panel title={`Node with ID "${data.id}" balance`} key={`node-balance-${data.id}`}>
          <Graph data={nodeDemands}
            timeChangeHandler={this.timeChangeHandler}
          />
        </Panel>
      </div>
    }
    else if (data.objectType === 'edge') {
      const branchData = await getBranchData(data.id);
      content = <Panel
        title={`Flows from Node with ID "${branchData.branch.source_node_id}" to Node with ID "${branchData.branch.target_node_id}"`}
        key={`branch-${data.id}`}
      >
        <Graph data={branchData.flows}
          timeChangeHandler={this.timeChangeHandler}
        />
      </Panel>
    }
    if (content) {
      this.setState({
        content,
      });
    }
  }

  render = () => {
    const { selectedTime, graphNodes, graphEdges, content } = this.state;
    return (
      <div className='App'>
        <div className='App-container'>
          <Panel title='Electrical Network'>
            <Info />
          </Panel>
          <Panel title='Time'>
            <TimeSelector
              timeChangeHandler={this.timeChangeHandler}
            />
          </Panel>
          {selectedTime ?
            <Panel title={`Graph showing hour "${selectedTime}"`}>
              <NetGraph
                graphNodes={graphNodes}
                graphEdges={graphEdges}
                graphClickHandler={this.graphClickHandler}
              />
            </Panel>
            : null}
          {content ?
            content
            : null}
          <Panel title='Author'>
            Patryk Bartoń <br />
            email: patryk.barton.pb@gmail.com
        </Panel>
        </div>
      </div >
    );
  }
}

export default App;
