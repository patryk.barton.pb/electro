import axios from 'axios';

export const get = (url, params) => {
    return axios.get(urlBuilder(url), params);
}

const urlBuilder = (url) => {
    // forces browser to fetch new data every request
    // (in case of runtime changes in DB)
    const currentTime = new Date().getTime();
    return `${process.env.REACT_APP_BACKEND_URL}${url}?time=${currentTime}`;
}