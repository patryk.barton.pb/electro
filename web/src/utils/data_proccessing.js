import { get } from './api';

const BALANCE_COMPUTATION_PRECISION = 8; 

const normalizeValues = (value, minIn, maxIn, minOut, maxOut) => {
  const rangeIn = maxIn - minIn;
  const rangeOut = maxOut - minOut;
  const part = (value - minIn) / rangeIn;
  return rangeOut * part + minOut;
}

const getNodesBalance = (branchesData) => {
  const nodesBalance = {};

  branchesData.map((branchData) => {
    const branch = branchData.branch;
    const flow = branchData.flow;
    if (!(branch.source_node_id in nodesBalance)) {
      nodesBalance[branch.source_node_id] = 0;
    }
    if (!(branch.target_node_id in nodesBalance)) {
      nodesBalance[branch.target_node_id] = 0;
    }
    nodesBalance[branch.source_node_id] -= flow.value;
    nodesBalance[branch.target_node_id] += flow.value;
  });

  return nodesBalance;
}

const processNegativeFlows = (branchesData) => {
  return branchesData.map((branchData) => {
    const branch = branchData.branch;
    const flow = branchData.flow;
    if (flow.value < 0) {
      [branch.source_node_id, branch.target_node_id] = [branch.target_node_id, branch.source_node_id];
    }
    flow.value = Math.abs(flow.value);
    return {
      branch,
      flow,
    }
  });
}

const adjustBalanceWithGenerationAndDemand = (balanceFromEdges, nodeData) => {
  let balance = balanceFromEdges - nodeData.demand.value;
  if (nodeData.node.type !== 1) {
    balance += nodeData.node.generation;
  }
  if (Math.abs(balance) < Math.pow(10, -1 * BALANCE_COMPUTATION_PRECISION)) {
    return 0;
  }
  return balance;
}

const getNodeSymbol = (nodeData) => {
  if (nodeData.node.type === 1) {
    return 'circle';
  }
  return 'rect';
}

const getNodeSize = (nodeData, minNodeValue, maxNodeValue) => {
  let value = nodeData.demand.value;
  if (nodeData.node.type !== 1) {
    value = Math.max(nodeData.node.generation - nodeData.demand.value, minNodeValue);
  }
  return parseInt(normalizeValues(value, minNodeValue, maxNodeValue, 15, 30));
}

export const getGraphData = async (selectedTime) => {
  const nodesData = (await get(`/nodes/${selectedTime}`)).data;
  const branchesData = (await get(`/branches/${selectedTime}`)).data;

  const branchDataNoNegative = processNegativeFlows(branchesData);

  const edgesValues = branchesData.map(el => el.flow.value);
  const maxEdgeValue = Math.max(...edgesValues);
  const minEdgeValue = Math.min(...edgesValues);

  const graphEdges = branchDataNoNegative.map((branchData) => {
    const branch = branchData.branch;
    const flow = branchData.flow;
    return {
      source: branch.source_node_id.toString(),
      target: branch.target_node_id.toString(),
      id: branch.id,
      value: flow.value,
      objectType: 'edge',
      symbol: flow.value === 0 ? null : ['none', 'arrow'],
      symbolSize: parseInt(normalizeValues(flow.value, minEdgeValue, maxEdgeValue, 5, 20)),
      lineStyle: {
        width: parseInt(normalizeValues(flow.value, minEdgeValue, maxEdgeValue, 2, 7)),
        opacity: 0.9,
        color: flow.value === 0 ? 'red' : 'rgba(0, 0, 0, 1)',
      },
      label: {
        show: true,
        formatter: edge => `${edge.value.toFixed(2)}`,
      },
    }
  });

  const nodesValues = nodesData.map(el => el.demand.value);
  const maxNodeValue = Math.max(...nodesValues);
  const minNodeValue = Math.min(...nodesValues);

  const nodesBalance = getNodesBalance(branchDataNoNegative);

  const graphNodes = nodesData.map((nodeData) => {
    const balance = adjustBalanceWithGenerationAndDemand(nodesBalance[nodeData.node.id], nodeData);
    let color = '#4db8ff';
    if (balance !== 0) {
      color = '#ff4d4d';
    }
    else if (nodeData.node.type === 1 && nodeData.demand.value === 0) {
      color = 'gray';
    }
    else if (nodeData.node.type !== 1) {
      color = '#fca103';
    }
    return {
      name: nodeData.node.id.toString(),
      demand: nodeData.demand.value,
      id: nodeData.node.id,
      balance: balance,
      objectType: nodeData.node.type === 1 ? 'node' : 'gen',
      generation: nodeData.node.generation,
      cost: nodeData.node.cost,
      symbol: getNodeSymbol(nodeData),
      symbolSize: getNodeSize(nodeData, minNodeValue, maxNodeValue),
      itemStyle: {
        color: color,
      },
    }
  });

  return {
    graphNodes,
    graphEdges,
  }
}

export const getNodeData = async nodeId => {
  return (await get(`/node/${nodeId}`)).data;
}

export const getBranchData = async branchId => {
  return (await get(`/branch/${branchId}`)).data;
}

export const getNodeBalance = async nodeId => {
  const { branches, demands, node } = (await get(`/node/${nodeId}/balance`)).data;
  const generation = node.generation ? node.generation : 0;
  const values = {};
  demands.map(demand => {
    values[demand.time] = generation - demand.value;;
  });
  branches.map(branch => {
    let direction = branch.source_node_id === nodeId ? -1 : 1;
    branch.flows.map(flow => {
      values[flow.time] += flow.value * direction;
    });
  });
  const result = [];
  for (let time in values) {
    if (Math.abs(values[time]) < Math.pow(10, -1 * BALANCE_COMPUTATION_PRECISION)) {
      result.push({
        time,
        value: values[time],
      });
    }
    else {
      result.push({
      time,
        value: values[time],
      });
    }
  }
  const sortedResult = result.sort((a,b) => a.time - b.time);
  return sortedResult;
}