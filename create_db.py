import h5py
import pathlib

from db import db
from models.node import NodeModel
from models.node_demand import NodeDemandModel
from models.generator import GeneratorModel
from models.branch import BranchModel
from models.branch_flow import BranchFlowModel


def create_nodes_and_generators_data(raw_gens, raw_nodes):
    visited_nodes = set()
    for gen_id, generation, cost in raw_gens:
        visited_nodes.add(gen_id)
        for node_id, node_type, demand in raw_nodes:
            if gen_id == node_id:
                gen = GeneratorModel(node_id, node_type, generation, cost)
                db.session.add(gen)
    for node_id, node_type, _ in raw_nodes:
        if node_id not in visited_nodes:
            node = NodeModel(node_id, node_type)
            db.session.add(node)
    db.session.commit()


def create_branches_data(raw_branches):
    for source_node_id, target_node_id, _ in raw_branches:
        branch = BranchModel(source_node_id, target_node_id)
        db.session.add(branch)
    db.session.commit()


def add_node_demands(time, raw_nodes):
    for node_id, _, demand in raw_nodes:
        node_demand = NodeDemandModel(node_id, time, demand)
        db.session.add(node_demand)
    db.session.commit()


def add_branches_flows(time, raw_branches):
    for source_node_id, target_node_id, flow in raw_branches:
        branch = BranchModel.find_by_source_and_target(source_node_id, target_node_id)
        flow = BranchFlowModel(branch.id, time, flow)
        db.session.add(flow)
    db.session.commit()


def create_db():
    db.create_all()
    file_name = 'data.hdf5'
    with h5py.File(file_name, 'r') as f:
        raw_data = f['results']
        single_hour_data = raw_data[list(raw_data.keys())[0]]
        create_nodes_and_generators_data(single_hour_data['gens'], single_hour_data['nodes'])
        create_branches_data(single_hour_data['branches'])
        for time_str in raw_data.keys():
            time = int(time_str.split('_')[1])
            time_data = raw_data[time_str]
            add_node_demands(time, time_data['nodes'])
            add_branches_flows(time, time_data['branches'])


def remove_db():
    db_file = pathlib.Path('db.sqlite')
    if db_file.is_file():
        db_file.unlink()
